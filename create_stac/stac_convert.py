""" _summary_

_extended_summary_

Returns:
    _type_: _description_
"""

import json

import pystac
import requests
from pystac import Catalog

from access_bucket.access_bucket import AccessBucket


class STACConvert:
    """STACConvert

    Args:
        bucket_path (str, optional): path of the bucket. Defaults to
    "https://pilot-imfe-o.s3-ext.jc.rl.ac.uk/haig-fras/".
        stac_path (str, optional): path of the stac. Defaults to "stac".
        stac_name (str, optional): name of the catalog main file. Defaults
    to "catalog.json".
    """

    def __init__(
        self,
        bucket_path: str = "https://pilot-imfe-o.s3-ext.jc.rl.ac.uk/haig-fras/",
        stac_path: str = "stac",
        stac_name: str = "catalog.json",
    ) -> None:
        self.bucket_path = bucket_path
        self.stac_path = stac_path
        self.stac_name = stac_name

        self.catalog = Catalog.from_file(
            f"{self.bucket_path}{self.stac_path}/{self.stac_name}"
        )
        self.layers = {}

    def create_item(self, item, base_root, asset_name):
        """create_item: create item for the output file

        Args:
            item (pyStac.Item): pystac Item
            base_root (str): name of the base root file
            asset_name (str): type of the asset

        Returns:
            dict: item for stac conversion
        """
        asset = item.assets[asset_name]
        old_item = item.to_dict()
        del old_item["assets"]
        del old_item["links"]
        old_item["asset"] = asset.to_dict()
        new_item = {
            "url": old_item["asset"]["href"],
            "data_type": old_item["asset"]["type"],
            "content": old_item["properties"].get("description"),
            "id": old_item["id"],
            "rel": asset_name,
        }
        if base_root == "bathymetry":
            new_item["get_value"] = {"depth": "m"}
        return new_item

    def get_json_info(self, unique_item: dict, root: str) -> dict:
        """get_json_info:

        Args:
            unique_item (dict): metadata information of the unique item
            root (str): name of the root collection

        Returns:
            dict: _description_
        """

        new_item = {
            "data_type": "Photo",
            "content": root.description,
            "protected": root.license,
            "url": unique_item["url"],
            "photos": [],
            "rel": unique_item["rel"],
        }
        response = requests.get(unique_item["url"], timeout=5)
        items_info = response.json()
        for item in items_info["features"]:
            item["properties"]["assets"] = item["assets"]
            item["properties"]["coordinates"] = item["geometry"]["coordinates"]
            new_item["photos"].append(item["properties"])
        return new_item

    def convert(self, exceptions: list = None) -> None:
        """convert: main function to convert stac to IMFE website layer.json
        format

        Args:
            exceptions (list, optional): a list of expections in the stac file
        conversion. Defaults to None.
        """

        if not exceptions:
            exceptions = ["get_depth"]
        layer0 = 0
        layer1 = 0
        layer2 = 0
        layer3 = 0

        for root, subcats, items in self.catalog.walk():
            # Root represents a catalog currently being walked in the tree
            if layer3 == layer2 and layer2 != 0:
                layer0 = 1
                layer2 = 0
                layer3 = 0
            if layer0 == 0:
                layer0 += 1
                for cat in subcats:
                    layer1 += 1
                    self.layers[cat.id] = {}
            elif layer0 == 1:
                layer0 += 1
                base_root = root.id
                print(base_root)
                self.layers[base_root]["layerNames"] = {}
                for cat in subcats:
                    layer2 += 1
                    self.layers[base_root]["layerNames"][cat.id] = {}
                layer3 = 0
            else:
                layer3 += 1
                flag = 0
                unique_item = []
                for item in items:
                    if item.assets:
                        if item.assets.get("image"):
                            if flag == 0:
                                self.layers[base_root]["layerNames"][root.id][
                                    "items"
                                ] = []
                                flag = 1
                            if (
                                item.assets["image"].media_type
                                == "application/geo+json"
                            ):
                                unique_item = self.create_item(item, base_root, "image")
                            else:
                                new_item = self.create_item(item, base_root, "image")
                                self.layers[base_root]["layerNames"][root.id][
                                    "items"
                                ].append(new_item)
                        if item.assets.get("overview"):
                            new_item = self.create_item(item, base_root, "overview")
                            self.layers[base_root]["layerNames"][root.id]["items"][-1][
                                "assetId"
                            ] = new_item["url"].split("/")[-1]
                    if item.links:
                        for link in item.links:
                            if link.rel in ("wms", "xyz"):
                                if flag == 0:
                                    self.layers[base_root]["layerNames"][root.id][
                                        "items"
                                    ] = []
                                    flag = 1
                                old_item = item.to_dict()
                                del old_item["assets"]
                                del old_item["links"]
                                old_item["asset"] = link.to_dict()

                                new_item = {
                                    "url": old_item["asset"]["href"],
                                    "data_type": old_item["asset"]["rel"],
                                    "content": old_item["properties"]["description"],
                                    "id": old_item["id"],
                                    "rel": link.rel,
                                }
                                if old_item["asset"].get("wms:layers"):
                                    new_item["params"] = {
                                        "layers": old_item["asset"].get("wms:layers")
                                    }
                                if old_item["asset"].get("wms:styles"):
                                    new_item["params"]["style"] = old_item["asset"].get(
                                        "wms:styles"
                                    )
                                self.layers[base_root]["layerNames"][root.id][
                                    "items"
                                ].append(new_item)
                if "items" in self.layers[base_root]["layerNames"][root.id].keys():
                    if unique_item:
                        selected_items = [self.get_json_info(unique_item, root)]
                    else:
                        selected_items = self.layers[base_root]["layerNames"][root.id][
                            "items"
                        ]
                    flag = 0
                    if len(selected_items) == 1:
                        for selected_item in selected_items:
                            if selected_item.get("rel") in ["image", "wms", "xyz"]:
                                self.layers[base_root]["layerNames"][root.id][
                                    "url"
                                ] = selected_item["url"]
                                self.layers[base_root]["layerNames"][root.id][
                                    "data_type"
                                ] = self.get_data_type(selected_item["data_type"])
                                self.layers[base_root]["layerNames"][root.id][
                                    "content"
                                ] = selected_item["content"]
                                protected = False
                                if root.license == "protected":
                                    protected = True
                                self.layers[base_root]["layerNames"][root.id][
                                    "protected"
                                ] = protected
                                if selected_item.get("photos"):
                                    self.layers[base_root]["layerNames"][root.id][
                                        "photos"
                                    ] = selected_item["photos"]
                                if selected_item.get("params"):
                                    self.layers[base_root]["layerNames"][root.id][
                                        "params"
                                    ] = selected_item["params"]
                                if "get_depth" in exceptions:
                                    if base_root.lower() == "bathymetry":
                                        self.layers[base_root]["layerNames"][root.id][
                                            "get_value"
                                        ] = {"depth": "m"}
                                if selected_item.get("assetId"):
                                    self.layers[base_root]["layerNames"][root.id][
                                        "assetId"
                                    ] = selected_item["assetId"]
                                    self.layers[base_root]["layerNames"][root.id][
                                        "threeD"
                                    ] = False
                        del self.layers[base_root]["layerNames"][root.id]["items"]
                    else:
                        for selected_item in selected_items:
                            self.layers[base_root]["layerNames"][
                                f"{root.id}-{selected_item['id']}"
                            ] = {}
                            self.layers[base_root]["layerNames"][
                                f"{root.id}-{selected_item['id']}"
                            ]["url"] = selected_item["url"]
                            self.layers[base_root]["layerNames"][
                                f"{root.id}-{selected_item['id']}"
                            ]["data_type"] = self.get_data_type(
                                selected_item["data_type"]
                            )
                            self.layers[base_root]["layerNames"][
                                f"{root.id}-{selected_item['id']}"
                            ]["content"] = selected_item["content"]
                            protected = False
                            if root.license == "protected":
                                protected = True
                            self.layers[base_root]["layerNames"][
                                f"{root.id}-{selected_item['id']}"
                            ]["protected"] = "protected"
                            if "get_depth" in exceptions:
                                if base_root.lower() == "bathymetry":
                                    self.layers[base_root]["layerNames"][
                                        f"{root.id}-{selected_item['id']}"
                                    ]["get_value"] = {"depth": "m"}
                            if selected_item.get("params"):
                                self.layers[base_root]["layerNames"][
                                    f"{root.id}-{selected_item['id']}"
                                ]["params"] = selected_item["params"]
                            if selected_item.get("assetId"):
                                self.layers[base_root]["layerNames"][
                                    f"{root.id}-{selected_item['id']}"
                                ]["assetId"] = selected_item["assetId"]
                                self.layers[base_root]["layerNames"][
                                    f"{root.id}-{selected_item['id']}"
                                ]["threeD"] = False

                        del self.layers[base_root]["layerNames"][root.id]

    def save_and_upload(
        self,
        filename: str = "layers.json",
        bucket_name: str = "haig-fras",
        bucket_folder: str = "frontend",
        aws_access_key_id=None,
        aws_secret_access_key=None,
        endpoint_url=None,
    ) -> None:
        """_summary_

        Args:
            filename (str, optional): file name of the output file. Defaults
        to "layers.json".
            bucket_name (str, optional): name of the bucket. Defaults to "haig-fras".
            bucket_folder (str, optional): folder in the bucket where the output
        will be saved. Defaults to "frontend".
            aws_access_key_id (str, optional): aws_access_key_id. Defaults to None.
            aws_secret_access_key (str, optional): aws_secret_access_key. Defaults to None.
            endpoint_url (str, optional): endpoint_url. Defaults to None.
        """

        with open(filename, "w", encoding="utf-8") as outfile:
            json.dump(self.layers, outfile)

        upload_bucket = AccessBucket(
            bucket=bucket_name,
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            endpoint_url=endpoint_url,
        )

        upload_bucket.upload(
            file_names=[filename], path="./", bucket_folder=bucket_folder, verbose=1
        )

    def get_data_type(self, datatype: str) -> str:
        """get_data_type: get data type based on pystac Media Type

        Args:
            datatype (str): pystac.MediaType

        Returns:
            str: data type
        """
        if datatype:
            if datatype == pystac.MediaType.GEOTIFF:
                datatype = "GEOTIFF"
            if datatype == pystac.MediaType.COG:
                datatype = "COG"
            if datatype == pystac.MediaType.PNG:
                datatype = "PNG"
            if datatype == pystac.MediaType.JPEG:
                datatype = "JPG"
            if datatype == pystac.MediaType.GEOJSON:
                datatype = "GeoJSON"
            if datatype == "wms":
                datatype = "wms"
            if datatype == "application/vnd.sqlite3; charset=binary":
                datatype = "MBTiles"
            if datatype == "xyz":
                datatype = "MBTiles"
        return datatype
