""" _summary_

_extended_summary_

Returns:
    _type_: _description_
"""

import json
import os
import shutil
from datetime import datetime
from urllib.parse import quote

import geopandas as gpd
import pystac
import requests
from dateutil.parser import parse
from shapely.geometry import Polygon, mapping, shape

from access_bucket.access_bucket import AccessBucket

TILE_SERVER = "https://imfe-pilot-tileserver.noc.ac.uk/"


class STACGen:
    """
    STACGEN: class to generate STAC Catalogs

    Args:
        metadata_path (str, optional): Metadata folder. Defaults to "metadatas".
        metadata_name (str, optional): Metadata name. Defaults to "main".
    """

    def __init__(
        self,
        metadata_path: str = "metadatas",
        metadata_name: str = "main",
    ) -> None:
        self.metadata_path = metadata_path
        if metadata_path.endswith("/"):
            self.metadata_path = self.metadata_path[:-1]
        self.metadata_name = metadata_name

        with open(
            f"{self.metadata_path}/{self.metadata_name}.json", encoding="utf-8"
        ) as metadata:
            self.metadata = json.load(metadata)
        self.catalog = pystac.Catalog(
            id=self.metadata["catalog"]["id"],
            description=self.metadata["catalog"]["id"],
            title=self.metadata["catalog"]["title"],
        )

    def stac_gen(
        self,
        stac_path: str = "stac",
        save_catalog: bool = True,
        upload_bucket: bool = False,
        aws_access_key_id: str = None,
        aws_secret_access_key: str = None,
        endpoint_url: str = None,
        bucket_name: str = "haig-fras",
        bucket_path: str = None,
    ) -> pystac.Catalog:
        """stac_gen: main funtion to generate the stac catalog

        Args:
            stac_path (str, optional): path where the stac catalog will be saved.
        Defaults to "stac".
            save_catalog (bool, optional): save catalog to the stac_path.
        Defaults to True.
            upload_bucket (bool, optional): upload stac on the object store.
        Defaults to False.
            aws_access_key_id (str, optional): aws_access_key_id. Defaults to None.
            aws_secret_access_key (str, optional): aws_secret_access_key.
        Defaults to None.
            endpoint_url (str, optional): endpoint_url. Defaults to None.
            bucket_name (str, optional): bucket_name. Defaults to "haig-fras".
            bucket_path (str, optional): bucket_path. Defaults to None.

        Returns:
            pystac.Catalog: STAC Catalog object
        """

        sub_catalogs = self.metadata.get("sub_catalogs")
        if sub_catalogs:
            for meta_catalog in sub_catalogs:
                print(meta_catalog)
                stac_gen_sub = STACGen(self.metadata_path, meta_catalog)
                sub_catalog = stac_gen_sub.stac_gen(save_catalog=False)
                self.catalog.add_child(sub_catalog)

        collections = self.metadata.get("collections")
        if collections:
            for meta_collection in collections:
                collection = self.create_collection(meta_collection)
                self.catalog.add_child(collection)

        if save_catalog:
            if os.path.isdir(stac_path):
                shutil.rmtree(stac_path)
            os.mkdir(stac_path)
            self.catalog.normalize_and_save(
                f"{os.getcwd()}/{stac_path}",
                catalog_type=pystac.CatalogType.SELF_CONTAINED,
            )
            if upload_bucket:
                upload_bucket = AccessBucket(
                    bucket=bucket_name,
                    aws_access_key_id=aws_access_key_id,
                    aws_secret_access_key=aws_secret_access_key,
                    endpoint_url=endpoint_url,
                )

                upload_bucket.rm(bucket_folder=stac_path, verbose=0, force=True)
                upload_bucket.upload(
                    path=stac_path, bucket_folder=bucket_path, verbose=1
                )

        return self.catalog

    def check_catalog(self) -> None:
        """check_catalog: print stac catalog results"""
        print(json.dumps(self.catalog.to_dict(), indent=4))

    def create_collection(self, metadata: str) -> pystac.Collection:
        """create_collection: create collection for catalog

        Args:
            metadata (str): name of the metadata file

        Returns:
            pystac.Collection: STAC Catalog Collection Object
        """
        items = []
        collection_footprint = None
        collection_datetime = []
        global_path = metadata.get("global_path")
        for meta_item in metadata["items"]:
            if meta_item.get("is_group"):
                items = self.create_items(items, meta_item, global_path)
            else:
                if meta_item.get("file"):
                    meta_item = self.generate_json(meta_item, global_path)
                item = self.create_item(meta_item, global_path)
                items.append(item)
        for item in items:
            if item.properties.get("datetime"):
                collection_datetime.append(parse(item.properties["datetime"]))
                collection_datetime.append(parse(item.properties["datetime"]))
            else:
                collection_datetime.append(parse(item.properties["start_datetime"]))
                collection_datetime.append(parse(item.properties["end_datetime"]))
            if not collection_footprint:
                collection_footprint = shape(item.geometry)
            else:
                collection_footprint = collection_footprint.union(shape(item.geometry))

        extent = self.create_extent(collection_datetime, collection_footprint)

        collection = pystac.Collection(
            id=metadata["id"],
            description=metadata["description"],
            title=metadata["title"],
            license=metadata["license"],
            extent=extent,
        )
        collection.add_items(items)

        if metadata.get("assets"):
            for meta_asset in metadata["assets"]:
                media_type = self.get_media_type(meta_asset["data_type"])
                asset = self.create_asset(
                    f"{metadata.get('global_path')}{meta_asset['path']}", media_type
                )
                collection.add_asset(meta_asset["asset_type"], asset)

        return collection

    def create_items(self, items: list, metadata: dict, global_path: str) -> list:
        """create_items: create items based on a group file

        Args:
            items (list): list of items
            metadata (dict): item metadata as a dict object
            global_path (str): path of the files on the object store (URL)

        Returns:
            list: list of dict items
        """
        response = requests.get(metadata["file"], timeout=5)
        items_info = response.json()["features"]

        for meta_item in items_info:
            coords = meta_item["geometry"]["coordinates"]
            meta_item = meta_item["properties"]
            meta_item["coords"] = {
                "min_long": coords[0],
                "min_lat": coords[1],
                "max_long": coords[0],
                "max_lat": coords[1],
            }
            meta_item["id"] = meta_item["filename"]
            meta_item["times"] = {
                "datetime": datetime.strftime(
                    datetime.now(), format="%Y-%m-%dT%H:%M:%SZ"
                )
            }
            meta_item["data_type"] = metadata["data_type"]
            extension = meta_item["data_type"]
            if meta_item["data_type"] == "COG":
                extension = "tif"
            meta_item["assets"] = [
                {
                    "data_type": metadata["thumbnail_format"],
                    "path": f"{global_path}{metadata['thumbnail_path']}/ \
                        {meta_item['filename']}.{metadata['thumbnail_format']}",
                    "asset_type": "thumbnail",
                }
            ]

            meta_item[
                "path"
            ] = f"{metadata['data_path']}/{meta_item['filename']}.{extension}"
            item = self.create_item(meta_item, global_path=global_path)
            item.to_dict()

            items.append(item)

        return items

    def create_item(self, metadata: dict, global_path: str = None) -> pystac.item.Item:
        """create_item: create stac item

        _extended_summary_

        Args:
            metadata (dict): metadata dict
            global_path (str, Optional): path of the files on the object store
        (URL). Default set to None.

        Returns:
            pystac.item.Item: pystac Item Object
        """

        coords = metadata.get("coords")

        if metadata["data_type"] != "WMS":
            metadata["path"] = f"{global_path}{metadata['path']}"

        if metadata.get("coords"):
            bbox = [
                coords["min_long"],
                coords["min_lat"],
                coords["max_long"],
                coords["max_lat"],
            ]
        else:
            bbox = self.get_bbox(metadata)
        footprint = self.get_footprint(bbox)
        times = metadata["times"]
        item_datetime = None
        if "datetime" in times.keys():
            item_datetime = parse(times["datetime"])

        item_start_datetime = None
        if "start_time" in times.keys():
            item_start_datetime = parse(times["start_time"])

        item_end_datetime = None
        if "end_time" in times.keys():
            item_end_datetime = parse(times["end_time"])
        item = pystac.Item(
            id=metadata["id"],
            geometry=footprint,
            bbox=bbox,
            datetime=item_datetime,
            start_datetime=item_start_datetime,
            end_datetime=item_end_datetime,
            properties={},
        )

        if metadata.get("description"):
            item.common_metadata.description = metadata["description"]

        if metadata.get("properties"):
            item = self.add_item_properties(item, metadata["properties"])

        if metadata["data_type"] != "WMS":
            media_type = self.get_media_type(metadata["data_type"])
            asset = self.create_asset(metadata["path"], media_type)
            item.add_asset("image", asset)

        if metadata.get("assets"):
            for meta_asset in metadata["assets"]:
                media_type = self.get_media_type(meta_asset["data_type"])
                asset = self.create_asset(meta_asset["path"], media_type)
                item.add_asset(meta_asset["asset_type"], asset)

        if metadata.get("links"):
            item = self.add_item_link(item, metadata["links"])

        return item

    def add_item_properties(
        self, item: pystac.item.Item, properties: dict
    ) -> pystac.item.Item:
        """add_item_properties: add aditional properties to item

        Args:
            item (pystac.item.Item): pyStac Item
            properties (dict): properties to be added to item

        Returns:
            pystac.item.Item: item with added properties
        """

        for prop_key, prop_value in properties.items():
            if prop_key == "gsd":
                item.common_metadata.gsd = prop_value
            if prop_key == "platform":
                item.common_metadata.platform = prop_value
            if prop_key == "instruments":
                item.common_metadata.instruments = prop_value
            if prop_key == "constellation":
                item.common_metadata.constellation = prop_value
            if prop_key == "mission":
                item.common_metadata.mission = prop_value
        return item

    def add_item_link(self, item: pystac.item.Item, links: list) -> pystac.item.Item:
        """add_item_link: add a link to a item

        Args:
            item (pystac.item.Item): pystac Item
            links (list): list of links that will be added to item

        Returns:
            pystac.item.Item: _description_
        """
        for meta_link in links:
            if meta_link.get("rel") == "wms":
                properties = {}
                if meta_link.get("wms:layers"):
                    properties["wms:layers"] = meta_link.get("wms:layers")
                if meta_link.get("wms:styles"):
                    properties["wms:styles"] = meta_link.get("wms:styles")
                link = pystac.Link(
                    rel=meta_link.get("rel"),
                    target=meta_link.get("href"),
                    title=meta_link.get("title"),
                    extra_fields=properties,
                )
            else:
                link = pystac.Link(
                    rel=meta_link.get("rel"),
                    target=meta_link.get("href"),
                    title=meta_link.get("title"),
                )

            media_type = self.get_media_type(meta_link.get("data_type"))
            if media_type:
                link.media_type = media_type

            # if meta_link.get('rel') == 'wms':
            #     properties = {
            #         'wms:layers': meta_link.get('wms:layers'),
            #         'wms:styles': meta_link.get('wms:styles')
            #     }
            #     item.extra_fields = properties

            item.add_link(link)
        return item

    def create_extent(
        self, collection_datetime: dict, collection_footprint: dict
    ) -> pystac.Extent:
        """create_extent: create a pyStac extent object

        Args:
            collection_datetime (dict): collection datetime
            collection_footprint (dict): collection footprint

        Returns:
            pystac.Extent: pystac Extent Object
        """

        # spatial_extent = pystac.SpatialExtent(bboxes=[collection_bbox])

        # if times.get('datetime'):
        #     start_time = datetime.strptime(times["datetime"], "%Y-%m-%dT%H:%M:%SZ")
        #     end_time = datetime.strptime(times["datetime"], "%Y-%m-%dT%H:%M:%SZ")
        # else:
        #     start_time = datetime.strptime(times["start_time"], "%Y-%m-%dT%H:%M:%SZ")
        #     end_time = datetime.strptime(times["end_time"], "%Y-%m-%dT%H:%M:%SZ")

        # temporal_extent = pystac.TemporalExtent(intervals=[[start_time, end_time]])

        # collection_extent = pystac.Extent(spatial=spatial_extent, temporal=temporal_extent)
        # return collection_extent

        collection_bbox = list(collection_footprint.bounds)
        spatial_extent = pystac.SpatialExtent(bboxes=[collection_bbox])
        collection_interval = sorted(collection_datetime)
        temporal_extent = pystac.TemporalExtent(intervals=[collection_interval])
        return pystac.Extent(spatial=spatial_extent, temporal=temporal_extent)

    def get_media_type(self, datatype: str) -> str:
        """get_media_type: get media type of the file

        Args:
            datatype (str): datatype of the file

        Returns:
            pystac.MediaType: pystac MediaType string
        """

        if datatype:
            datatype = datatype.lower()
            if datatype == "geotiff":
                datatype = pystac.MediaType.GEOTIFF
            if datatype == "cog":
                datatype = pystac.MediaType.COG
            if datatype == "png":
                datatype = pystac.MediaType.PNG
            if datatype in ("jpg", "jpeg"):
                datatype = pystac.MediaType.JPEG
            if datatype == "CSV":
                datatype = "text/csv"
            if datatype == "mbtiles":
                datatype = "application/vnd.sqlite3; charset=binary"
            if datatype == "geojson":
                datatype = pystac.MediaType.GEOJSON
        return datatype

    def create_asset(
        self, item_path: str, media_type: pystac.MediaType = pystac.MediaType.GEOTIFF
    ) -> pystac.Asset:
        """create_asset: create pystac Asset

        Args:
            item_path (str): path of the item
            media_type (pystac.MediaType): media type of the file. Defaults to
        pystac.MediaType.GEOTIFF.

        Returns:
            pystac.Asset: pystac Asset Object
        """

        return pystac.Asset(href=item_path, media_type=media_type)

    def get_bbox(self, metadata: dict) -> list:
        """get_bbox: get bbox of a COG file
        Args:
            metadata (dict): metadata information about the COG file

        Returns:
            list: list of bbox limits
        """
        raster = quote(metadata["path"], safe="()*!'")

        if metadata.get("signed_url"):
            raster = quote(metadata["signed_url"], safe="()*!'") + "&encoded=true"

        response = requests.get(f"{TILE_SERVER}cog/info?url={raster}", timeout=10)
        cog_info = response.json()

        bounds = cog_info["bounds"]

        return bounds

    def get_footprint(self, bounds: list) -> dict:
        """get_footprint: get footprint of a COG file

        Args:
            bounds (list): limits of the bounds

        Returns:
            dict: dict object of a Polygon
        """
        footprint = Polygon(
            [
                [bounds[0], bounds[1]],
                [bounds[0], bounds[3]],
                [bounds[2], bounds[3]],
                [bounds[2], bounds[1]],
            ]
        )

        return mapping(footprint)

    def generate_json(self, metadata: str, global_path: str) -> dict:
        """generate_json: generate a geojson file from the csv data

        Args:
            metadata (dict): metadata dict
            global_path (str, Optional): path of the files on the object store
        (URL). Default set to None.

        Returns:
            dict: a dict that represent the geoJSON file
        """

        response = requests.get(metadata["file"], timeout=5)
        filename = metadata["path"].split("/")[-1]
        items_info = response.json()

        for idx, feature in enumerate(items_info["features"]):
            feature["assets"] = {}
            if metadata.get("thumbnail_path"):
                feature["assets"]["thumbnail"] = {}
                feature["assets"]["thumbnail"][
                    "href"
                ] = f"{global_path}{metadata['thumbnail_path']}/ \
                    {feature['properties']['filename']}.{metadata['thumbnail_format']}"
                feature["assets"]["thumbnail"]["type"] = self.get_media_type(
                    metadata["thumbnail_format"]
                )
            if metadata.get("image_path"):
                feature["assets"]["image"] = {}
                feature["assets"]["image"][
                    "href"
                ] = f"{global_path}{metadata['image_path']}/ \
                    {feature['properties']['filename']}.{metadata['thumbnail_format']}"
                feature["assets"]["image"]["type"] = self.get_media_type(
                    metadata["image_format"]
                )

            if len(feature.get("assets").keys()) > 0:
                items_info["features"][idx] = feature

        with open(filename, "w", encoding="utf-8") as f:
            json.dump(items_info, f)

        bucker_folder = "/".join(metadata["path"].split("/")[2:-1])
        bucket = metadata["path"].split("/")[1]
        upload_bucket = AccessBucket(bucket=bucket)
        upload_bucket.upload(
            path="./", file_names=[filename], bucket_folder=bucker_folder, verbose=0
        )
        gdf = gpd.read_file(f"{global_path}{metadata['path']}")
        coords = gdf.total_bounds
        metadata["coords"] = {
            "min_long": coords[0],
            "min_lat": coords[1],
            "max_long": coords[2],
            "max_lat": coords[3],
        }

        os.remove(filename)

        return metadata
